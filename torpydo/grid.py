from enum import Enum
from typing import Tuple, List
from colorama import Fore, Back, Style

from torpydo.ship import Position


class CellState(Enum):
    EMPTY = ' '
    SHIP_PRESENT = Style.BRIGHT + 'S' + Style.RESET_ALL
    SHIP_HIT = Style.BRIGHT + Fore.RED + 'X' + Style.RESET_ALL
    SHIP_MISS = Style.BRIGHT + Fore.CYAN + 'O' + Style.RESET_ALL
    SHIP_SUNK = Style.BRIGHT + Fore.YELLOW + '=' + Style.RESET_ALL


class Grid:
    def __init__(self, size: int):
        self.size = size
        self.grid = self._init_grid()

    def _init_grid(self) -> List[List[CellState]]:
        return [[CellState.EMPTY] * self.size for _ in range(self.size)]

    @staticmethod
    def _resolve_position(position: Position) -> Tuple[int, int]:
        row = position.row - 1
        col = position.column.value - 1

        return row, col

    def _get(self, row, col):
        return self.grid[row][col]

    def get_cell(self, position: Position) -> CellState:
        row, col = self._resolve_position(position)
        return self._get(row, col)

    def set_cell(self, position: Position, cell_state: CellState):
        row, col = self._resolve_position(position)
        self.grid[row][col] = cell_state

    def paint(self):
        cell_width = 4

        # Column Letters
        for i in range(self.size):
            print(f'  {chr(65+i)} ', end='')
        print()

        # Create Grid
        for row in range(self.size):
            print('-' * (cell_width*self.size + 1))
            for col in range(self.size):
                c = self._get(row, col).value
                print(f"| {c} ", end="")
            print(f"|  {row+1}")
        print('-' * (4*self.size + 1))

