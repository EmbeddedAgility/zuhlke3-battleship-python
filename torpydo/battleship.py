import random
import os
import time
import webbrowser
import colorama
import platform
from typing import Tuple
from colorama import Fore, Back, Style
import sys

from torpydo.ship import Color, Letter, Position, Ship
from torpydo.game_controller import GameController, PlayerType

GRID_SIZE = 8

myFleet = []
enemyFleet = []
game_controller: GameController = GameController(grid_size=GRID_SIZE)
player_enemy_scores = [0,0]


def main():
    colorama.init()
    print(Fore.YELLOW + r"""
                                    |__
                                    |\/
                                    ---
                                    / | [
                             !      | |||
                           _/|     _/|-++'
                       +  +--|    |--|--|_ |-
                     { /|__|  |/\__|  |--- |||__/
                    +---------------___[}-_===_.'____                 /\
                ____`-' ||___-{]_| _[}-  |     |_[___\==--            \/   _
 __..._____--==/___]_|__|_____________________________[___\==--____,------' .7
|                        Welcome to Battleship                         BB-61/
 \_________________________________________________________________________|""" + Style.RESET_ALL)

    initialize_game()

    start_game()


def take_turn(hit_target: Position,
              hit_message: Tuple[str, str],
              miss_message: Tuple[str, str],
              player: PlayerType) -> str:
    hit_color = hit_message[1]
    miss_color = miss_message[1]

    fleet = enemyFleet if player == PlayerType.USER else myFleet
    is_hit = game_controller.apply_hit(fleet, hit_target, player)

    if is_hit:
        if player == PlayerType.USER:
            player_enemy_scores[0] += 1
        else:
            player_enemy_scores[1] += 1
        return hit_color + hit_message[0] + Style.RESET_ALL
    else:
        return miss_color + miss_message[0] + Style.RESET_ALL


def start_end_sequence():
    print('.', end='', flush=True)
    time.sleep(1.5)
    print('.', end='', flush=True)
    time.sleep(1.5)
    print('.', end='', flush=True)
    print()

    time.sleep(4)
    print('Wait for it....')
    time.sleep(4)
    print('Just a bit more....')
    time.sleep(4)

    if player_enemy_scores[0] >= 17:  # Won
        webbrowser.open("https://www.youtube.com/watch_popup?v=mTMaL2zkGas")
    else:  # Lost
        webbrowser.open("https://www.youtube.com/watch_popup?v=oavMtUWDBTM&ab_channel=RealPapaPit")


def cheat_code(next_user_input):
    if next_user_input == "win":
        player_enemy_scores[0] = 17
        return True
    elif next_user_input == "lose":
        player_enemy_scores[1] = 17
        return True
    else:
        return False



def start_game():
    global myFleet, enemyFleet, game_controller
    # clear the screen
    if platform.system().lower() == "windows":
        cmd = 'cls'
    else:
        cmd = 'clear'
    os.system(cmd)

    msg_user = ""
    msg_enemy = ""

    while True:
        os.system(cmd)
        game_controller.update_ui()
        print()
        print(msg_user)
        print()
        print(msg_enemy)
        print()
        print(f"{Fore.CYAN}Current score: Player({player_enemy_scores[0]}) : Enemy({player_enemy_scores[1]})" + Style.RESET_ALL)
        print()

        if check_if_game_ends():
            start_end_sequence()
            break

        # Player
        print("Player, it's your turn")
        while True:
            next_user_input = input("Enter coordinates for your shot :")
            if cheat_code(next_user_input):
                break
            position = parse_position(next_user_input)
            if position.row < 1 or position.row > GRID_SIZE or Letter(position.column).value < 1 or Letter(position.column).value > GRID_SIZE:
                print("Shooting out of field. Try again")
                continue
            else:
                break

        msg_user = take_turn(position,
                             hit_message=(f"Player -> {position.column.name}{position.row} hit!", Fore.GREEN),
                             miss_message=(f"Player -> {position.column.name}{position.row} miss", Fore.YELLOW),
                             player=PlayerType.USER)

        # Enemy
        position = get_random_position()
        msg_enemy = take_turn(position,
                              hit_message=(f"Computer -> {position.column.name}{position.row} hit!", Fore.RED),
                              miss_message=(f"Computer -> {position.column.name}{position.row} miss", Fore.GREEN),
                              player=PlayerType.ENEMY)


def check_if_game_ends():
    if player_enemy_scores[0] >= 17:
        print(Fore.GREEN + Style.BRIGHT + "You have won! Congratulations!!" + Style.RESET_ALL)
        return True
    elif player_enemy_scores[1] >= 17:
        print(Fore.RED + Style.BRIGHT + "You have lost! Better luck next time!" + Style.RESET_ALL)
        return True
    return False


def parse_position(input: str):
    letter = Letter[input.upper()[:1]]
    number = int(input[1:])
    position = Position(letter, number)

    return Position(letter, number)


def get_random_position():
    rows = 8
    lines = 8

    letter = Letter(random.randint(1, lines))
    number = random.randint(1, rows)
    position = Position(letter, number)

    return position


def initialize_game():
    initialize_myFleet()
    initialize_enemyFleet()

def is_ship_overlapping(fleet, ship, index):

    for i in range(index):
        if len(list(set(fleet[i].positions) & set(ship.positions))) != 0:
            return True

    return False


def initialize_myFleet():
    global myFleet

    myFleet = game_controller.initialize_ships()

    print("Please position your fleet (Game board has size from A to H and 1 to 8) :")

    fleet_index = 0
    while fleet_index < len(myFleet):
        print()
        print(f"Please enter the initial positions for the {myFleet[fleet_index].name} (size: {myFleet[fleet_index].size})")

        while True:
            init_position_input = input(f"Enter initial position of {myFleet[fleet_index].name} (i.e A3) (Type exit or quit to quit the game):")
            if init_position_input == 'exit' or init_position_input == 'quit':
                sys.exit("Exiting game")
            result = myFleet[fleet_index].add_top_position(init_position_input)
            if result:
                break

            print('Invalid position. Please try again.')

        while True:
            direction_input = input(f"Enter direction of {myFleet[fleet_index].name} (t, b, l, r) (Type exit or quit to quit the game):")
            if direction_input == 'exit' or direction_input == 'quit':
                sys.exit("Exiting game")
            result = myFleet[fleet_index].add_direction(direction_input)
            if result:
                break

            print('Invalid direction. Please try again.')

        print('ship', myFleet[fleet_index].positions)
        if is_ship_overlapping(myFleet, myFleet[fleet_index], fleet_index):
            print('Ships are overlapping. Try again')
        else:
            fleet_index += 1


    game_controller.fill_player_ships(myFleet)


def initialize_enemyFleet():
    global enemyFleet

    enemyFleet = game_controller.initialize_ships()

    enemyFleet[0].positions.append(Position(Letter.B, 4))
    enemyFleet[0].positions.append(Position(Letter.B, 5))
    enemyFleet[0].positions.append(Position(Letter.B, 6))
    enemyFleet[0].positions.append(Position(Letter.B, 7))
    enemyFleet[0].positions.append(Position(Letter.B, 8))

    enemyFleet[1].positions.append(Position(Letter.E, 5))
    enemyFleet[1].positions.append(Position(Letter.E, 6))
    enemyFleet[1].positions.append(Position(Letter.E, 7))
    enemyFleet[1].positions.append(Position(Letter.E, 8))

    enemyFleet[2].positions.append(Position(Letter.A, 3))
    enemyFleet[2].positions.append(Position(Letter.B, 3))
    enemyFleet[2].positions.append(Position(Letter.C, 3))

    enemyFleet[3].positions.append(Position(Letter.F, 8))
    enemyFleet[3].positions.append(Position(Letter.G, 8))
    enemyFleet[3].positions.append(Position(Letter.H, 8))

    enemyFleet[4].positions.append(Position(Letter.C, 5))
    enemyFleet[4].positions.append(Position(Letter.C, 6))

if __name__ == '__main__':
    main()
