import random
from enum import Enum
from typing import List

from colorama import Fore, Back, Style

from torpydo.grid import Grid, CellState
from torpydo.ship import Color, Letter, Position, Ship


class PlayerType(Enum):
    USER = 1
    ENEMY = 2


class GameController(object):

    def __init__(self, grid_size=8):
        self.player_grid: Grid = Grid(size=grid_size)
        self.enemy_grid: Grid = Grid(size=grid_size)

    def apply_hit(self, ships: list, shot: Position, player: PlayerType):
        current_grid = self.player_grid if player == PlayerType.ENEMY else self.enemy_grid

        if ships is None:
            raise ValueError('ships is null')

        if shot is None:
            raise ValueError('shot is null')

        for ship in ships:
            for position in ship.positions:
                if position == shot:
                    if ship.hit_ship(shot):
                        if ship.ship_sunk:
                            for sunk_ship_position in ship.positions:
                                current_grid.set_cell(sunk_ship_position, CellState.SHIP_SUNK)
                        else:
                            current_grid.set_cell(shot, CellState.SHIP_HIT)
                        return True
                    return False

        current_grid.set_cell(shot, CellState.SHIP_MISS)
        return False

    def initialize_ships(self):
        return [
            Ship("Aircraft Carrier", 5, Color.CADET_BLUE),
            Ship("Battleship", 4, Color.RED),
            Ship("Submarine", 3, Color.CHARTREUSE),
            Ship("Destroyer", 3, Color.YELLOW),
            Ship("Patrol Boat", 2, Color.ORANGE)]

    def is_ship_valid(self, ship: Ship):
        is_valid = len(ship.positions) == ship.size
        
        return is_valid

    @staticmethod
    def get_random_position(size: int):
        letter = random.choice(list(Letter))
        number = random.randrange(size)
        position = Position(letter, number)

        return position

    def update_ui(self):
        print()

        print(Style.BRIGHT + 'Player Grid' + Style.RESET_ALL)
        self.player_grid.paint()

        print()

        print(Style.BRIGHT + 'Enemy Grid'+ Style.RESET_ALL)
        self.enemy_grid.paint()

    def fill_player_ships(self, fleet: List[Ship]):
        for ship in fleet:
            for position in ship.positions:
                self.player_grid.set_cell(position, CellState.SHIP_PRESENT)


