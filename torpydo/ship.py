from enum import Enum

class Color(Enum):
    CADET_BLUE = 1
    CHARTREUSE = 2
    ORANGE = 3
    RED = 4
    YELLOW = 5

class Letter(Enum):
    A = 1
    B = 2
    C = 3
    D = 4
    E = 5
    F = 6
    G = 7
    H = 8
    I = 9
    J = 10
    K = 11

class Position(object):
    def __init__(self, column: Letter, row: int):
        self.column = column
        self.row = row

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def __str__(self):
        return f"{self.column.name}{self.row}"

    def __hash__(self):
        return 100*self.column.value + self.row

    __repr__ = __str__

class Ship(object):
    def __init__(self, name: str, size: int, color: Color):
        self.name = name
        self.size = size
        self.color = color
        self.positions = []
        self.sunk_positions = []
        self.ship_sunk = False

    def add_position(self, input: str):
        letter = Letter[input.upper()[:1]]
        number = int(input[1:])
        position = Position(letter, number)

        self.positions.append(Position(letter, number))

    def hit_ship(self, position: Position):
        if position not in self.sunk_positions:
            self.sunk_positions.append(position)
            if len(self.sunk_positions) >= self.size:
                self.ship_sunk = True
            return True
        return False


    def add_top_position(self, top_position: str):
        try:
            letter = Letter[top_position.upper()[:1]]
            number = int(top_position[1:])
            if number > 8 or number < 1:
                return False
            position = Position(letter, number)
            self.top_left_position = position
        except:
            return False
        return True

    def add_direction(self, direction: str):
        self.positions = []
        self.direction = direction
        init_position = self.top_left_position
        if direction == 't' or direction == 'b':
            increment = 1 if direction == 'b' else -1
            if (init_position.row + increment*self.size) > 8 or (init_position.row + increment*self.size) < 1:
                return False
            for i in range(self.size):
                position = Position(init_position.column, init_position.row + increment*i)
                self.positions.append(position)
        elif direction == 'l' or direction == 'r':
            increment = 1 if direction == 'r' else -1
            if (Letter(init_position.column).value + (increment*self.size)) > 8 or (Letter(init_position.column).value + (increment*self.size)) < 1:
                return False
            for i in range(self.size):
                position = Position(Letter(Letter(init_position.column).value + (increment*i)), init_position.row)
                self.positions.append(position)
        else:
            return False
        return True

    def __str__(self):
        return f"{self.color.name} {self.name} ({self.size}): {self.positions}"

    __repr__ = __str__
